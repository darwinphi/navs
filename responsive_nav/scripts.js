let navToggle = document.getElementById('js-navToggle')
let navLists = document.getElementById('js-navLists')

navToggle.addEventListener('click', function() {
	navLists.classList.toggle('js-open')
})
